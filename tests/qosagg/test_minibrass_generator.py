import textwrap
from io import StringIO

from qosbench.qosagg.minibrass_generator import MiniBrassGenerator, MiniBrassGeneratorConfig


def test_minibrass_generator(output: StringIO) -> None:
    config = MiniBrassGeneratorConfig(3, 7)
    MiniBrassGenerator(config, output).generate()

    assert output.getvalue() == textwrap.dedent("""\
    include "defs.mbr";
            
    PVS: cfn1 = new CostFunctionNetwork("cfn1") {
    \tk: '210';
    \tsoft-constraint sc0: 'floor(wf_aggregated_attr_0)';
    \tsoft-constraint sc1: 'floor(wf_aggregated_attr_1)';
    \tsoft-constraint sc2: 'floor(wf_aggregated_attr_2)';
    };
    
    solve cfn1;
    """)  # noqa: W293
