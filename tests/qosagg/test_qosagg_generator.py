import textwrap
from io import StringIO

from qosbench.qosagg.qosagg_generator import QosAggGenerator, QosAggGeneratorConfig


def test_qosagg_generator(output: StringIO) -> None:
    config = QosAggGeneratorConfig(2, 2, 2, 2, 2, 2, 1)
    QosAggGenerator(config, output).generate()

    assert output.getvalue() == textwrap.dedent("""\
    workflow {
    \tgraph: ((A B | C D) -> (E F + G H))^2;
    
    \taggregation attr_0: sum, max;
    
    \tprovision a0 for A: attr_0 = 3.5011;
    \tprovision a1 for A: attr_0 = 8.7576;
    \tprovision b0 for B: attr_0 = 3.6907;
    \tprovision b1 for B: attr_0 = 1.7277;
    \tprovision c0 for C: attr_0 = 7.5902;
    \tprovision c1 for C: attr_0 = 7.6296;
    \tprovision d0 for D: attr_0 = 6.3316;
    \tprovision d1 for D: attr_0 = 3.9929;
    \tprovision e0 for E: attr_0 = 5.3869;
    \tprovision e1 for E: attr_0 = 7.5739;
    \tprovision f0 for F: attr_0 = 7.1705;
    \tprovision f1 for F: attr_0 = 0.5822;
    \tprovision g0 for G: attr_0 = 5.7660;
    \tprovision g1 for G: attr_0 = 9.8504;
    \tprovision h0 for H: attr_0 = 8.7430;
    \tprovision h1 for H: attr_0 = 0.6672;
    };
    """)  # noqa: W293
