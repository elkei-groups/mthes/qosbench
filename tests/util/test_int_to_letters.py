import pytest

from qosbench.util.int_to_letters import int_to_letters


@pytest.mark.parametrize(("number", "letters"), [
    (0, 'a'),
    (1, 'b'),
    (-4, '-e'),
    (25, 'z'),
    (26, 'aa'),
    (27, 'ab'),
    (51, 'az'),
    (52, 'ba'),
    (730, 'abc'),
    (-28, '-ac'),
])
def test_int_to_letters(number, letters):
    assert int_to_letters(number) == letters
