from setuptools import setup, find_packages

with open("README.md") as f:
    readmeText = f.read()

version = 1.0

setup(
    name='qosbench',
    version=version,
    description='Benchmarking Tool for QosAgg and Qos2Mzn',
    long_description=readmeText,
    long_description_content_type="text/markdown",
    license_files=['LICENSE'],
    author='Elias Keis',
    author_email='git-commits@elkei.de',
    url='https://gitlab.com/elkei-groups/mthes/qosbench',
    packages=find_packages(exclude=['tests', 'tests.*', 'docs']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': ['qosbench=qosbench.app:run'],
    },
    install_requires=[
        'matplotlib',
        'numpy'
    ],
    tests_require=[
        'pytest',
        'pytest-mock'
    ]
)
