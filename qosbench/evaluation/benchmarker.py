import math
import tempfile
import textwrap
from typing import Iterable, IO

from qosbench.evaluation import reporting
from qosbench.evaluation.minibrass_executor import MiniBrassExecutor
from qosbench.evaluation.minizinc_executor import MiniZincExecutor
from qosbench.evaluation.qosagg_executor import QosAggExecutor
from qosbench.evaluation.reporting import BenchmarkReporter, ConsoleBenchmarkReporter
from qosbench.qosagg.minibrass_generator import MiniBrassGenerator, MiniBrassGeneratorConfig
from qosbench.qosagg.qosagg_generator import QosAggGeneratorConfig, QosAggGenerator

_floor_function = textwrap.dedent("""\
    function var int: floor(var float: x) =
    \tlet {
    \t\tvar int: c;
    \t\tconstraint c <= x ∧ x < c + 1;
    \t} in c;
    """)


class Benchmarker:
    def __init__(self, qag_exec: QosAggExecutor, mzn_exec: MiniZincExecutor, mbr_exec: MiniBrassExecutor,
                 reporter: BenchmarkReporter = ConsoleBenchmarkReporter()):
        self.qag_exec = qag_exec
        self.mzn_exec = mzn_exec
        self.mbr_exec = mbr_exec
        self.reporter = reporter

    def benchmark(self, generator_configs: Iterable[QosAggGeneratorConfig]) -> bool:
        self.reporter.prepare()
        all_timed_out = True
        for config in generator_configs:
            with tempfile.NamedTemporaryFile("wt", prefix="qosbench_", suffix=".qag") as qag_file, \
                    tempfile.NamedTemporaryFile("wt", prefix="qosbench_wf_", suffix=".mzn") as wf_file:
                # QosAgg
                QosAggGenerator(config, qag_file).generate()
                qosagg_time = self.qag_exec.execute(qag_file.name, wf_file.name)

                # MiniZinc
                with tempfile.NamedTemporaryFile("wt", prefix="qosbench_cm_", suffix=".mzn") as cm_file:
                    objective = " + ".join([f"wf_aggregated_attr_{i}" for i in range(config.attributes)])
                    self._prepare_constraint_model(cm_file, wf_file.name, objective)
                    mzn_time = self.mzn_exec.solve(cm_file.name)

                # MiniBrass
                with tempfile.NamedTemporaryFile("wt", prefix="qosbench_cm_", suffix=".mzn") as cm_file, \
                        tempfile.NamedTemporaryFile("wt", prefix="qosbench_", suffix=".mbr") as mbr_file:
                    mbr_config = MiniBrassGeneratorConfig(config.attributes, config.total_number_of_task_instances)
                    MiniBrassGenerator(mbr_config, mbr_file).generate()
                    self._prepare_constraint_model(cm_file, wf_file.name, "topLevelObjective", _floor_function)
                    mbr_time = self.mbr_exec.solve(cm_file.name, mbr_file.name)

                # Reporting
                times = reporting.ExecutionTimes(qag_time=qosagg_time, mzn_time=mzn_time, mbr_time=mbr_time)
                all_timed_out = all_timed_out and any(t == math.inf for t in times)
                self.reporter.report(config, times)
        return all_timed_out

    @classmethod
    def _prepare_constraint_model(cls, file: IO, wf_file_name: str, objective: str, additional_code: str = None) \
            -> None:
        if additional_code:
            file.write(additional_code + "\n")
        file.write(textwrap.dedent(f"""\
            include "{wf_file_name}";
            solve minimize {objective};
            """))
        file.flush()
