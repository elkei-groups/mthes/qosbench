import math
import subprocess
import sys
import time
from typing import Optional, Iterable


class MiniBrassExecutor:
    def __init__(self, mbr_exec: Iterable[str] = ("minibrass",), timeout: Optional[float] = None):
        self.cmd = mbr_exec
        self.timeout = timeout

    def solve(self, mzn_file: str, mbr_file: str) -> float:
        start = time.perf_counter()
        try:
            proc = subprocess.run(args=[*self.cmd, mbr_file, mzn_file], stdout=subprocess.PIPE, timeout=self.timeout,
                                  stderr=subprocess.PIPE, text=True)
            if proc.returncode != 0 or proc.stderr != "" or "Selection graph" not in proc.stdout:
                print(proc.stderr, file=sys.stderr)
                return math.nan
        except subprocess.TimeoutExpired:
            return math.inf
        except subprocess.CalledProcessError:
            return math.nan
        return time.perf_counter() - start
