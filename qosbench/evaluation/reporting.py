import abc
import csv
import itertools
import sys
from abc import abstractmethod
from typing import TextIO, List, NamedTuple, Optional, Tuple, Union, Callable

from qosbench.qosagg.qosagg_generator import QosAggGeneratorConfig


class ExecutionTimes(NamedTuple):
    qag_time: float
    mzn_time: Union[float, Tuple[float, Optional[float]]]
    mbr_time: float


def _to_row(config: QosAggGeneratorConfig) -> List:
    return [config.parallel, config.tasks_per_parallel, config.choices, config.tasks_per_choice, config.iterations,
            config.provisions_per_task, config.attributes, config.seed]


class BenchmarkReporter(abc.ABC):
    def __init__(self, separate_flattening: bool = False):
        self._columns = ["parallel", "tpp", "choices", "tpc", "iterations", "ppt", "attributes", "seed", "qosagg time",
                         *(["mzn time"] if not separate_flattening else ["mzn flat time", "mzn solve time"]),
                         "mbr time"]
        self._times_to_row: Callable[[ExecutionTimes], List[float]]
        if not separate_flattening:
            self._times_to_row = (lambda times: [*times])  # type: ignore
        else:
            self._times_to_row = (lambda times: [times.qag_time, times.mzn_time[0], times.mzn_time[1], times.mbr_time])  # type: ignore  # noqa: E501

    @abstractmethod
    def prepare(self):
        pass

    @abstractmethod
    def report(self, config: QosAggGeneratorConfig, times: ExecutionTimes):
        pass


class CsvBenchmarkReporter(BenchmarkReporter):
    def __init__(self, file: TextIO, separate_flattening: bool = False):
        super().__init__(separate_flattening=separate_flattening)
        self._writer = csv.writer(file)
        self.add_header = not file.tell()

    def prepare(self):
        if self.add_header:
            self._writer.writerow(self._columns)
            self.add_header = False

    def report(self, config: QosAggGeneratorConfig, times: ExecutionTimes):
        self._writer.writerow(_to_row(config) + self._times_to_row(times))


class ConsoleBenchmarkReporter(BenchmarkReporter):
    def prepare(self):
        sys.stdout.write("\t".join(self._columns) + "\n")

    def report(self, config: QosAggGeneratorConfig, times: ExecutionTimes):
        config_values = [str(v) for v in _to_row(config)]
        time_values = [f"{v:.5f}" for v in self._times_to_row(times)]
        sys.stdout.write("\t".join(itertools.chain(config_values, time_values)))
