import math
import subprocess
import tempfile
import time
from typing import Optional, Iterable, Tuple, Callable, Union, List


class MiniZincExecutor:
    def __init__(self, mzn_exec: Iterable[str] = ("minizinc",), timeout: Optional[float] = None,
                 separate_flattening: bool = False):
        self.minizinc_cmd = list(mzn_exec)
        self.timeout = timeout
        self.solve: Callable[[str], Union[float, Tuple[float, Optional[float]]]] = \
            self._solve_at_once if not separate_flattening else self._solve_with_flattening  # type: ignore

    def _solve_with_flattening(self, in_file: str) -> Tuple[float, Optional[float]]:
        with tempfile.NamedTemporaryFile("wt", suffix=".fzn", prefix="qosbench_") as fzn_file:
            short_command = [self.minizinc_cmd[0]]  # flattening can not understand some params, e.g., -p 7
            try:  # add "--solver xyz" if it is part of the command
                solver_param_index = self.minizinc_cmd.index("--solver")
                short_command += [self.minizinc_cmd[solver_param_index], self.minizinc_cmd[solver_param_index + 1]]
            except (ValueError, IndexError):
                pass
            flattening_time = self._run_mzn(args=[*short_command, "-c", "--fzn", fzn_file.name,
                                                  "--no-output-ozn", in_file])
            if not math.isfinite(flattening_time):
                return flattening_time, None
            solving_time = self._run_mzn(args=[*self.minizinc_cmd, fzn_file.name])
            return flattening_time, solving_time

    def _solve_at_once(self, in_file: str) -> float:
        return self._run_mzn(args=[*self.minizinc_cmd, in_file])

    def _run_mzn(self, args: List[str], **kwargs) -> float:
        try:
            start = time.perf_counter()
            result = subprocess.run(args=args, stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, timeout=self.timeout,
                                    text=True, **kwargs)
            duration = time.perf_counter() - start
            if result.returncode:
                raise subprocess.CalledProcessError(result.returncode, result.args, result.stdout, result.stderr)
            return duration
        except subprocess.TimeoutExpired:
            return math.inf
        except subprocess.CalledProcessError as e:
            self._print_mzn_error(e)
            return math.nan

    @classmethod
    def _print_mzn_error(cls, e: subprocess.CalledProcessError):
        error_lines = e.stderr.strip().split("\n") if e.stderr else None
        error_msg = error_lines[-1] if error_lines else "(empty stderr)"
        print(f"MIniZinc failed: {error_msg}")
