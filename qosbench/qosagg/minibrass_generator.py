import textwrap
from typing import IO, NamedTuple, Iterable


class MiniBrassGeneratorConfig(NamedTuple):
    attributes: int
    task_instances: int


class MiniBrassGenerator:
    def __init__(self, config: MiniBrassGeneratorConfig, output: IO[str]) -> None:
        self.output = output
        self.config = config

    def generate(self) -> None:
        soft_constraints = "".join([f"\t{sc}\n" for sc in self._soft_constraints])
        self.output.write(textwrap.dedent(f"""\
            include "defs.mbr";
            
            PVS: cfn1 = new CostFunctionNetwork("cfn1") {{
            \tk: '{self._k}';
            """))  # noqa: W293
        self.output.write(soft_constraints)
        self.output.write(textwrap.dedent("""\
            };
            
            solve cfn1;
            """))  # noqa: W293
        self.output.flush()

    @property
    def _soft_constraints(self) -> Iterable[str]:
        for i, attribute in enumerate(self._attributes):
            yield f"soft-constraint sc{i}: 'floor({attribute})';"

    @property
    def _attributes(self) -> Iterable[str]:
        for i in range(self.config.attributes):
            yield f"wf_aggregated_attr_{i}"

    @property
    def _k(self) -> int:
        return self.config.task_instances * self.config.attributes * 10
