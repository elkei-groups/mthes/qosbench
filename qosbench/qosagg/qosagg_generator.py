import random
from collections import defaultdict
from typing import NamedTuple, Iterable, Mapping, Set, IO, Hashable

from qosbench.util.int_to_letters import int_to_letters


class QosAggGeneratorConfig(NamedTuple):
    tasks_per_parallel: int
    tasks_per_choice: int
    parallel: int
    choices: int
    iterations: int
    provisions_per_task: int
    attributes: int
    seed: Hashable = 0

    @property
    def total_number_of_tasks(self) -> int:
        return self.tasks_per_parallel * self.parallel + self.tasks_per_choice * self.choices

    @property
    def total_number_of_task_instances(self) -> int:
        return self.total_number_of_tasks * self.iterations


class QosAggGenerator:
    def __init__(self, config: QosAggGeneratorConfig, output: IO[str]) -> None:
        self.config = config
        self.output = output
        self._random = random.Random(hash(config))
        self._task_names = TaskNameProvider()

    def generate(self) -> None:
        self.output.write("workflow {\n")
        self._generate_graph()
        self.output.write("\n")
        attributes = self._generate_attributes()
        self.output.write("\n")
        self._generate_provisions(self._task_names.previous_task_names, attributes)
        self.output.write("};\n")
        self.output.flush()

    def _generate_graph(self) -> None:
        parallels = self._get_nested_composition(self.config.tasks_per_parallel, self.config.parallel, " | ")
        choices = self._get_nested_composition(self.config.tasks_per_choice, self.config.choices, " + ")
        graph = f"(({parallels}) -> ({choices}))^{self.config.iterations}"
        self.output.write(f"\tgraph: {graph};\n")

    def _get_nested_composition(self, sequence_length: int, branches: int, combinator: str):
        return self._get_composition(
            [self._get_composition(self._task_names.get_task_names(sequence_length), " ") for _ in range(branches)],
            combinator
        )

    @classmethod
    def _get_composition(cls, subgraphs: Iterable[str], combinator: str):
        joint = combinator.join(subgraphs)
        return joint if joint else "null"

    def _generate_provisions(self, tasks: Iterable[str], attributes: Iterable[str]) \
            -> Mapping[str, Iterable[str]]:
        provisions_by_task: Mapping[str, Set[str]] = defaultdict(lambda: set())
        for task in tasks:
            for provision in [f"{task.lower()}{i}" for i in range(self.config.provisions_per_task)]:
                provisions_by_task[task].add(provision)
                attribute_values = ", ".join(f"{attr} = {self._random.random() * 10:.4f}" for attr in attributes)
                self.output.write(f"\tprovision {provision} for {task}: {attribute_values};\n")
        return provisions_by_task

    def _generate_attributes(self) -> Iterable[str]:
        attributes = [f"attr_{i}" for i in range(self.config.attributes)]
        for attribute in attributes:
            self.output.write(f"\taggregation {attribute}: sum, max;\n")
        return attributes


class TaskNameProvider:
    def __init__(self):
        self.next_index: int = 0

    @classmethod
    def _to_task_name(cls, i: int) -> str:
        return int_to_letters(i).upper()

    def get_task_name(self) -> str:
        index = self.next_index
        self.next_index += 1
        return self._to_task_name(index)

    def get_task_names(self, count: int) -> Iterable[str]:
        for i in range(count):
            yield self.get_task_name()

    @property
    def previous_task_names(self) -> Iterable[str]:
        return [self._to_task_name(i) for i in range(self.next_index)]
