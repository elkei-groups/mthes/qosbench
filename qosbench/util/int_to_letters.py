def _non_negative_int_to_letters(number: int) -> str:
    base = ord('z') - ord('a') + 1
    result = ""
    while True:
        remainder = number % base
        result = chr(ord('a') + remainder) + result
        number = number // base
        if number == 0:
            break
        number -= 1  # aa != a even if a is similar to 0
    return result


def int_to_letters(number: int) -> str:
    if number < 0:
        return '-' + _non_negative_int_to_letters(-number)
    else:
        return _non_negative_int_to_letters(number)
