import argparse
import itertools
from typing import Iterator, Iterable, Tuple

from qosbench.evaluation.benchmarker import Benchmarker
from qosbench.evaluation.minibrass_executor import MiniBrassExecutor
from qosbench.evaluation.minizinc_executor import MiniZincExecutor
from qosbench.evaluation.qosagg_executor import QosAggExecutor
from qosbench.evaluation.reporting import CsvBenchmarkReporter, ConsoleBenchmarkReporter
from qosbench.plotting import plotter
from qosbench.qosagg.qosagg_generator import QosAggGeneratorConfig, QosAggGenerator


def run():
    parser = _get_parser()

    args: argparse.Namespace = parser.parse_args()
    args.mzn_exec = args.mzn_exec if "mzn_exec" in args and args.mzn_exec else ["minizinc"]
    args.qosagg_exec = args.qosagg_exec if "qosagg_exec" in args and args.qosagg_exec else ["qosagg"]
    args.mbr_exec = args.mbr_exec if "mbr_exec" in args and args.mbr_exec else ["minibrass"]
    args.func(args)


def _get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    subcommands = parser.add_subparsers(help='subcommand', required=True)

    qosagg_parser = subcommands.add_parser('qosagg')
    qosagg_subcommands = qosagg_parser.add_subparsers(help="subsubcommand", required=True)

    qosagg_gen_parser = qosagg_subcommands.add_parser('generate', parents=[_get_qosagg_gen_parent(False)],
                                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    qosagg_gen_parser.add_argument('out_file', type=argparse.FileType('w', encoding='UTF-8'), metavar="out",
                                   help="output QosAgg file")
    qosagg_gen_parser.set_defaults(func=generate_qosagg)

    qosagg_transf_parent = argparse.ArgumentParser(add_help=False)
    qosagg_transf_parent.add_argument('-Q', '--qosagg-exec', dest="qosagg_exec", help="QosAgg command", type=str,
                                      action="extend", nargs="+")
    mzn_solve_parent = argparse.ArgumentParser(add_help=False)
    mzn_solve_parent.add_argument('-M', '--mzn-exec', dest="mzn_exec", help="MiniZinc command",
                                  type=str, action="extend", nargs='+')
    mzn_solve_parent.add_argument('--separate-flattening', dest="separate_flattening",
                                  action=argparse.BooleanOptionalAction, default=False,
                                  help="measure time for flattening and solving separately")
    mbr_solve_parent = argparse.ArgumentParser(add_help=False)
    mbr_solve_parent.add_argument('-B', '--mbr-exec', dest="mbr_exec", help="MiniBrass command",
                                  type=str, action="extend", nargs='+')
    timeout_parent = argparse.ArgumentParser(add_help=False)
    timeout_parent.add_argument('--timeout', default=15, type=float,
                                help="timeout for subprocess calls in seconds")

    qosagg_transf_parser = qosagg_subcommands.add_parser('transform', parents=[qosagg_transf_parent, timeout_parent],
                                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    qosagg_transf_parser.add_argument('in_file', metavar="in", help="input QosAgg file")
    qosagg_transf_parser.add_argument('out_file', metavar="out", help="output MiniZinc file")
    qosagg_transf_parser.set_defaults(func=transform_qosagg)

    qosagg_bench_parser = qosagg_subcommands.add_parser('benchmark', parents=[_get_qosagg_gen_parent(True),
                                                                              qosagg_transf_parent,
                                                                              mbr_solve_parent,
                                                                              mzn_solve_parent,
                                                                              timeout_parent],
                                                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    qosagg_bench_parser.add_argument('--out', help="output CSV file", dest="out_file",
                                     type=argparse.FileType('a', bufsize=1))
    qosagg_bench_parser.set_defaults(func=benchmark_qosagg)

    qosagg_plot_parser = subcommands.add_parser('plot', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    qosagg_plot_parser.add_argument('in_file', metavar='in', help="input CSV file",
                                    type=argparse.FileType('r', encoding="UTF-8"))
    qosagg_plot_parser.add_argument('-o', '--out_file', metavar='out',
                                    help="write image to this file instead of opening interactive view",
                                    type=str)
    qosagg_plot_parser.add_argument('-x', type=str, help="data column on x-axis", required=True)
    qosagg_plot_parser.add_argument('-y', type=str, help="data column on y-axis (enables 3D plot)",
                                    required=False, default=None)
    qosagg_plot_parser.add_argument('-t', '--time-column', type=str, help="limit the plotted time columns",
                                    required=False, default=None, dest="time_columns", nargs="*")
    qosagg_plot_parser.add_argument('-f', '--filter', type=_check_filter, nargs='*', default=[],
                                    help="ignore rows not matching filter criteria (format: column=value)")
    qosagg_plot_parser.add_argument('--param-values', dest="show_params", action=argparse.BooleanOptionalAction,
                                    default=True, help="show the values of all parameters in the bottom left corner")
    qosagg_plot_parser.set_defaults(func=plot_qosagg)

    return parser


def _get_qosagg_gen_parent(allow_ranges: bool) -> argparse.ArgumentParser:
    check_not_negative = _check_non_negative_range if allow_ranges else _check_not_negative
    check_positive = _check_positive_range if allow_ranges else _check_positive
    to_default = (lambda i: (i,)) if allow_ranges else lambda i: i

    qosagg_gen_parent = argparse.ArgumentParser(add_help=False)
    qosagg_gen_parent.add_argument('--parallel', help="number of parallel branches", type=check_not_negative,
                                   default=to_default(3))
    qosagg_gen_parent.add_argument('--tpp', help="number of tasks per parallel branch",
                                   type=check_not_negative, default=to_default(3))
    qosagg_gen_parent.add_argument('--choices', help="number of choices", type=check_not_negative,
                                   default=to_default(3))
    qosagg_gen_parent.add_argument('--tpc', help="number of tasks per choice", type=check_not_negative,
                                   default=to_default(3))
    qosagg_gen_parent.add_argument('--iterations', help="number of iterations", type=check_positive,
                                   default=to_default(2))
    qosagg_gen_parent.add_argument('--ppt', help="number of provisions per task", type=check_positive,
                                   default=to_default(3))
    qosagg_gen_parent.add_argument('--attributes', type=check_positive, default=to_default(2),
                                   help="number of attributes")
    qosagg_gen_parent.add_argument('--seed', type=check_not_negative, default=to_default(0),
                                   help="seed for the random workflow generation")
    return qosagg_gen_parent


def _check_positive(value) -> int:
    i_value = int(value)
    if i_value <= 0:
        raise argparse.ArgumentTypeError("%s is not a positive integer" % value)
    return i_value


def _check_not_negative(value) -> int:
    i_value = int(value)
    if i_value < 0:
        raise argparse.ArgumentTypeError("%s is negative" % value)
    return i_value


def _check_positive_range(value) -> Iterable[int]:
    for v in _check_range(value):
        yield _check_positive(v)


def _check_non_negative_range(value) -> Iterable[int]:
    for v in _check_range(value):
        yield _check_not_negative(v)


def _check_range(value) -> Iterable[int]:
    try:
        return int(value),
    except ValueError:
        pass
    parts = [int(s) for s in str(value).split(":")]
    if len(parts) not in (2, 3):
        raise argparse.ArgumentTypeError("%s is neither a fixed value nor a integer range" % value)
    return range(*parts)


def _check_filter(value) -> Tuple[str, int]:
    try:
        key, val = str(value).split("=")[:2]
        return key, int(val)
    except ValueError:
        raise argparse.ArgumentTypeError("%s is not a valid filter specification" % value)


def generate_qosagg(args: argparse.Namespace) -> None:
    config = _args_to_qosagg_generator_config(args)
    QosAggGenerator(config, args.out_file).generate()


def transform_qosagg(args: argparse.Namespace) -> None:
    QosAggExecutor(args.qosagg_exec, args.timeout).execute(args.in_file, args.out_file)


def benchmark_qosagg(args: argparse.Namespace) -> None:
    reporter = CsvBenchmarkReporter(args.out_file, args.separate_flattening) \
        if args.out_file else ConsoleBenchmarkReporter(args.separate_flattening)
    Benchmarker(reporter=reporter,
                qag_exec=QosAggExecutor(args.qosagg_exec, args.timeout),
                mzn_exec=MiniZincExecutor(args.mzn_exec, args.timeout, args.separate_flattening),
                mbr_exec=MiniBrassExecutor(args.mbr_exec, args.timeout)) \
        .benchmark(_args_to_qosagg_generator_configs(args))


def plot_qosagg(args: argparse.Namespace) -> None:
    filters = {t[0]: t[1] for t in args.filter}
    plotter.plot(input_file=args.in_file, filters=filters, x_column=args.x, add_hint=args.show_params,
                 output_file=args.out_file, y_column=args.y, time_columns=args.time_columns)


def _args_to_qosagg_generator_configs(args: argparse.Namespace) -> Iterator[QosAggGeneratorConfig]:
    for config_values in itertools.product(args.tpp, args.tpc, args.parallel, args.choices, args.iterations, args.ppt,
                                           args.attributes, args.seed):
        yield QosAggGeneratorConfig(*config_values)


def _args_to_qosagg_generator_config(args: argparse.Namespace) -> QosAggGeneratorConfig:
    return QosAggGeneratorConfig(tasks_per_parallel=args.tpp,
                                 tasks_per_choice=args.tpc,
                                 parallel=args.parallel,
                                 choices=args.choices,
                                 iterations=args.iterations,
                                 provisions_per_task=args.ppt,
                                 attributes=args.attributes,
                                 seed=args.seed)


if __name__ == '__main__':
    run()
